import chaospy as cp
import numpy as np
import matplotlib.pyplot as plt


u18_measure = np.loadtxt('../Data/u18.txt', delimiter=',');
AUData = np.loadtxt('../Data/AU.txt', delimiter=',');
AUData[0,0] = 0
AUData[0,1] = 0;

T = np.linspace(0, 700, 701)
AU = np.interp(T,AUData[:,0],AUData[:,1]);
AUData = np.zeros((T.size,2))
AUData[:,0] = T
AUData[:,1] = AU


def model(x,C,eta,T,AUData):
    gamma = 20.3 # kN/m3
    phi = 28*np.pi/180. # friction angle(o)
    c = 14.8 # kPa, cphesion
    alpha = 30*np.pi/180. # slope(o)
    H = 20 # m, depth
    S = 8 # m
    L = 135 # m

    dT = 1
    AUt = AUData[:,1]
    AUxdT_cumu = np.cumsum(AUt*dT)
    U = ((gamma*H*(np.sin(alpha)-np.cos(alpha)*np.tan(phi))-c)/eta*T
         -AUxdT_cumu/eta/L/S)  - AUt/C/eta/L/S*(x**2/2-L*x+16*L**2/np.pi**3*np.sin(np.pi/2/L*x)*np.exp(-C*np.pi**2/4/L**2*T))
    U = U*1000 # mm
    return U

x = 41
y = model(x, 88, 3600, T, AUData)

cp.seed(1000)
distribution = cp.J(cp.Uniform(87, 89),cp.Uniform(3050, 4150))
samples = distribution.sample(1000, "H")
evals = [model(x, node[0], node[1], T, AUData) for node in samples.T]
polynomial_expansion = cp.orth_ttr(3, distribution)
foo_approx = cp.fit_regression(polynomial_expansion, samples, evals)
expected = cp.E(foo_approx, distribution)
deviation = cp.Std(foo_approx, distribution)



plt.plot(u18_measure[:,0],u18_measure[:,1],'ro')
plt.plot(T,y,'k-')
plt.plot(T,expected,'b-')
plt.plot(T,expected+deviation,'r--')
plt.plot(T,expected-deviation,'r--')
plt.legend(['Measured','c=88, eta=3600','Mean, PCE','Std, PCE'], loc=4)
plt.ylabel('Time')
plt.ylabel('U18')
plt.savefig('../Results/u18.pdf')
plt.show()
