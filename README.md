### What is this repository for? ###

This repositiory serves as a hub for a landslide project.


### Structure of folders ###

* Code
* Data
* Results
* Manuscript
* References

### Download ###

* [here](https://bitbucket.org/happycharleswang/landslide-2018/downloads/)

